﻿#include <iostream>

const int N = 5;

int main()
{
    int arr[N][N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            arr[i][j] = i + j;
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int row_index = buf.tm_mday % N;

    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += arr[row_index][j];
    }
    std::cout << "Sum of elements in row " << row_index << ": " << sum << std::endl;

    return 0;
}
